angular.module('oauthModule', [
    'oauth1Client'
])

.config(function(oauth1ClientProvider) {
    oauth1ClientProvider.config({
        consumerKey: '1fuYx4bx8EOM',
        consumerSecret: 'LfOMpIOO8MqySE8RUpUINopGUKHRYe0wbZfk76FNCO9UvW5j',
        requestEndpoint: 'http://wp-test.local.com/oauth1/request',
        authorizeEndpoint: 'http://wp-test.local.com/oauth1/authorize',
        accessEndpoint: 'http://wp-test.local.com/oauth1/access',
        oauthCallback: 'http://wp-test.local.com/oauth1/a/'
    });
});

angular.module('starter.controllers', ['oauthModule'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, oauth1Client) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  $scope.oauth1 = oauth1Client;
  $scope.test = null; //oauth1Client.authorize();
  $scope.vartest = null;
  
  


  

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    $scope.test = $scope.oauth1.authorize();

    $scope.test.then(function(authorizedHttp) {
     
    authorizedHttp({
        method: "POST",
        url: "http://robtown.com/wp-json/wp/v2/posts",
        
        data: {
           title: "Foo title",
           content: "Foo content",
           status: "publish"
        }
    })

    .then(function(response) {
        alert(response.message);
    }, function(response) {
        alert("Error creating post! " + JSON.stringify(response));
        return false;
    });
    
});
    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    // $timeout(function() {
    //  $scope.closeLogin();
    // }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});



